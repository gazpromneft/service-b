package ru.gazpromneft.processors;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j2;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.http.entity.ContentType;
import ru.gazpromneft.model.JsonFaultMessage;


@Log4j2
/**
 * Процессор-перехватчик и обёрточник всех возникших исключений, проброшенных на уровень приложения.
 * Необходим для внятного и наглядного представления исключения в REST-ответе в формате json.
 */
public class ErrorProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        Gson gson = new Gson();
        String camelContextPath = exchange.getIn().getHeader("CamelServletContextPath", String.class);
        if (log.isDebugEnabled()) {
            log.debug("CamelServletContextPath" + camelContextPath);
        }
        Throwable t = (Throwable) exchange.getProperty("CamelExceptionCaught");
        JsonFaultMessage exception;

        exception = new JsonFaultMessage(500, t.toString(), t);

        String sException = gson.toJson(exception, JsonFaultMessage.class);
        if (log.isDebugEnabled()) {
            log.debug("JSONException is " + sException);
        }
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setBody(sException, String.class);
        exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 500);
        exchange.getOut().setHeader(Exchange.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString());
        exchange.getOut().removeHeader("SOAPAction");
    }
}
