package ru.gazpromneft.processors;

import lombok.extern.log4j.Log4j2;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.time.LocalDateTime;

@Log4j2
public class MonitoringProcessor implements Processor {

    private MessageCounter messageCounter;

    public MonitoringProcessor(MessageCounter messageCounter) {
        this.messageCounter = messageCounter;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        Long counter = messageCounter.getCount();
        log.info("Message count: " + counter);
        LocalDateTime time = LocalDateTime.now();
        exchange.getIn().setHeader("XXX-MES-COUNT", counter);
        exchange.getIn().setHeader("XXX-MES-START", time.toString());
    }
}
