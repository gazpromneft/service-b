package ru.gazpromneft.processors;

import lombok.extern.log4j.Log4j2;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicLong;

@Log4j2
public class MessageCounter {
    private static final AtomicLong messageCounter = new AtomicLong(0L);

    public Long getCount(){
     return messageCounter.incrementAndGet();
    }

    public void logNow(){
       log.info("From AMQ timer: " +  LocalDateTime.now());
    }

    public void logSource(){
        log.info("Timer in source: " +  LocalDateTime.now());
    }

    public LocalDateTime getTime(){
        return LocalDateTime.now();
    }

}
