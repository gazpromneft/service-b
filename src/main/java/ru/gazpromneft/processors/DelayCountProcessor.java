package ru.gazpromneft.processors;

import lombok.extern.log4j.Log4j2;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Log4j2
public class DelayCountProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {

        Long counter = (Long) exchange.getIn().getHeader("XXX-MES-COUNT");
        String startTime = (String) exchange.getIn().getHeader("XXX-MES-START");
        LocalDateTime parsed = LocalDateTime.parse(startTime);
        log.info("XXX-MES-COUNT: " + counter + " XXX-MES-START: " + parsed);
        LocalDateTime endTime = LocalDateTime.now();
        log.info("End time: " + endTime);
        log.info("Message: "+counter +  " Delay : " + ChronoUnit.MILLIS.between(parsed, endTime));
    }
}
