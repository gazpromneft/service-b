package ru.gazpromneft;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ComponentScan(basePackages = "ru.gazpromneft")
@EnableConfigurationProperties
@ImportResource({"classpath:camel-context.xml"})
@Log4j2
public class Application {
    private static String serviceVersion;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        log.info("App version: " + serviceVersion);
    }

    //Необходимо для подстановки версии приложения при старте
    @Value("${service.version}")
    private synchronized void setVersion(String version) {
        Application.serviceVersion = version;
    }
}
