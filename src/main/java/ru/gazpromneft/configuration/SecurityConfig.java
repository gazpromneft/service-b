package ru.gazpromneft.configuration;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

import java.util.Map;

/**
 * Конфигурация спринг-секурити. Используется basic авторизация и NoOpPasswordEncoder.
 */

@Configuration
@EnableWebSecurity
@Log4j2
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${secret.encoderId}")
    private String encoderId;

    @Value("#{${service.users.map}}")
    private Map<String, String> usersMap;



    @Bean
    public UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        for (Map.Entry<String, String> entry : usersMap.entrySet()) {
            manager.createUser(User
                    .withUsername(entry.getKey())
                    .password(passwordEncoder().encode(entry.getValue()))
                    .roles("USER").build());
        }

        return manager;
    }

    /**
     * Конфигурация доступа.
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        http.addFilterBefore(filter, CsrfFilter.class);
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/rest/status/readiness").permitAll()
                .antMatchers("/rest/status/liveness").permitAll()
                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("/").hasRole("USER")
                .anyRequest().authenticated()
                .and().httpBasic();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

}
